from collections import Counter
import time
from typing import Any, Dict, List

import uvicorn
from fastapi import FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware
import numpy as np
import sklearn.datasets as ds
import sklearn.preprocessing as pr
from sklearn.tree import DecisionTreeClassifier, DecisionTreeRegressor
from sklearn.ensemble import RandomForestClassifier, RandomForestRegressor
import sklearn.metrics as m
from sklearn.model_selection import train_test_split
from pydantic import BaseModel
from sklearn.utils.multiclass import type_of_target


class PipelinePiece(BaseModel):
    content: str
    id: str
    name: str


app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["http://localhost:5173"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

DATASETS = {
    "load_iris": ds.load_iris,
    "diabets": ds.load_diabetes,
    "wine": ds.load_wine,
}
PREPROCESSORS = {
    "standard": pr.StandardScaler,
    "minmax": pr.MinMaxScaler,
    "normalizer": pr.Normalizer,
}
MODELS = {
    "decision_tree_classifier": DecisionTreeClassifier,
    "decision_tree_regressor": DecisionTreeRegressor,
    "random_forest_classifier": RandomForestClassifier,
    "random_forest_regressor": RandomForestRegressor,
}
METRICS = {
    "r2": m.r2_score,
    "MSE": m.mean_squared_error,
    "precision": m.precision_score,
    "recall": m.recall_score,
    "f1": m.f1_score,
}

AVAILABLE = {
    "datasets": list(DATASETS.keys()),
    "preprocessors": list(PREPROCESSORS.keys()),
    "models": list(MODELS.keys()),
    "metrics": list(METRICS.keys()),
}


CORRECT_ORDER = ["datasets", "preprocessors", "models", "metrics"]


@app.get("/")
def alive():
    return {
        "time": time.time(),
    }


@app.get("/available_elements/")
def available_elements():
    return [{"name": k, "methods": v} for k, v in AVAILABLE.items()]


def is_pipeline_valid(pieces: List[PipelinePiece]) -> bool:
    correct_orders = [
        ["datasets", "preprocessors", "models", "metrics"],
        ["datasets", "models"],
        ["datasets", "preprocessors", "models"],
        ["datasets", "models", "metrics"],
    ]
    names = Counter(map(lambda x: x.name, pieces))
    # Allow only a single dataset and a single model
    if names["datasets"] != 1 or names["models"] != 1:
        return False
    # Counter keeps the insertion order
    got_order = list(map(lambda s: s.lower(), names.keys()))
    for order in correct_orders:
        if got_order == order:
            return True

    return False


@app.post("/compute_pipeline")
def compute_pipeline(pieces: List[PipelinePiece]):
    print("Computing pipeline")
    if not is_pipeline_valid(pieces):
        return HTTPException(
            status_code=500, detail="The pipeline is not in the correct order!"
        )
    assert len(pieces) >= 2  # at least dataset and model
    # Extract the dataset
    X, y = DATASETS[pieces.pop(0).content](return_X_y=True)
    # Compute the preprocessing
    while pieces[0].name == "preprocessors":
        piece = pieces.pop(0)
        X = PREPROCESSORS[piece.content]().fit_transform(X)
    # Train the model
    model = MODELS[pieces.pop(0).content]()
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.2, random_state=42
    )
    model.fit(X_train, y_train)
    pred = model.predict(X_test)
    print(pred, y_test)
    # Compute the metrics
    results = []
    while len(pieces) > 0 and pieces[0].name == "metrics":
        piece = pieces.pop(0)
        results.append(compute_metric(piece.content, y_test, pred, type_of_target(y)))
    return {"status_code": 200, "detail": results}


def is_classification_metrics(fn_name):
    return fn_name in {"precision", "recall", "accuracy", "f1"}


def compute_metric(
    fn_name: str, y_true: np.ndarray, pred: np.ndarray, problem_type: str
) -> Dict[str, Any]:
    additional_args = {}
    if is_classification_metrics(fn_name):
        # If not binary (i.e, multiclass), use macro averaging
        if problem_type != "binary":
            problem_type = "macro"
        additional_args["average"] = problem_type
    return {
        "name": fn_name,
        "result": METRICS[fn_name](
            y_true,
            pred,
            **additional_args,
        ),
    }


if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=9000, reload=True)
