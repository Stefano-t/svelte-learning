# Svelte + Vite = Machine Learning

Simple full-stack project to learn frontend dev. The project provides a simple (and ugly) interface to train already-defined machine learning model on already-created datasets.

## Requirements

In order to replicate this project, you'll only need:

- [python](https://www.python.org/)
- [npm](https://www.npmjs.com/)


## How to build the project

### Backend

Go inside the `backend` folder. Create a virtual env with the following command:

```bash
$ python3 -m venv .venv
```

Then source the env and install dependencies:

```bash
$ source .venv/bin/actiavate
$ pip install -r requirements.txt
```

### Frontend

Go inside the `frontend` folder. Then, install the frontend dependencies with the following command:

```bash
$ npm build
```


## How to run the application


Launch the `start.sh` script. Make sure it's executable. To stop the application, run the `stop.sh` script.


## How the backend works

The backend exposes some functions directly taken from scikit-learn. What it does is basically wrapping these functions and exposing them to the frontend. For these reason, the user freedom will be strongly limited by scikit-learn functionality. All the datasets will be split using a random train-test split with test size of 20%.


## What you can do

- drag and drop datasets, models, preprocessors and metrics
- exchange elements in the drag area
- compute the pipeline and see the result
- clear the drag area


## What you cannot do

- add parameters to the single items
- fine-tune a model
- customize the train-test split
- customize preprocessors


## Resourses

- [Svelte](https://svelte.dev/)
- [Vite](https://vitejs.dev/)
- [FastAPI](https://fastapi.tiangolo.com/)
- [scikit-learn](https://scikit-learn.org/stable/)
