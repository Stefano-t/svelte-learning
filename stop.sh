#!/bin/bash

ps -ef | pgrep "backend/main.py" | xargs kill
ps -ef | pgrep "npm run dev" | xargs kill -9
