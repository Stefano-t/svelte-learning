#!/bin/bash

source ./backend/.venv/bin/activate
python3 backend/main.py &
cd frontend && npm run dev &
